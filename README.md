# README #
### What is this repository for? ###

Simple Prime number web service REST API using Spring framework

### How do I get set up? ###
Make sure you have Spring installed. 
Run RestApiApplication.java

Then use Postman
Ask if "number" is a prime:
Get Request
http://localhost:8080/prime/isprime/{number}

To get all added primes:
Get Request
http://localhost:8080/prime/getprimes

To get all added non primes:
Get Request
http://localhost:8080/prime/getnonprimes

To add a prime:
Post Request
http://localhost:8080/prime/itsaprime/{number}

To add a non prime:
http://localhost:8080/prime/notaprime/{number}


