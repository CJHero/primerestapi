package com.example.restApi;

public interface PrimeRepository {
  public boolean isItAPrime(int number);
}
