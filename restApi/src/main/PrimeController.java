package com.example.restApi;


import java.util.ArrayList;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PrimeController {
  PrimeRepository pr;

  public PrimeController(PrimeRepository pr) {
    this.pr = pr;
  }

  private ArrayList<Integer> notPrimes = new ArrayList<Integer>();
  private ArrayList<Integer> primes = new ArrayList<Integer>();

  @GetMapping("prime/isprime/{number}")
  public String isPrime(@PathVariable("number") int number) {
    if (notPrimes.contains(number))
      return " " + number + " is not a prime";
    if (primes.contains(number))
      return "Its a prime! " + number + " can only be divided by itself!";


    return "I dont have it in my memory. Please calculate for me and post it and I will not forget it! :D";

  }

  @RequestMapping(value = "/prime/itsaprime/{number}", method = RequestMethod.POST)
  public String prime(@PathVariable("number") int number) {
    if (!(primes.contains(number))) {
      primes.add(number);
      return "Its added to the prime database";
    }
    return "its already added!";
  }

  @RequestMapping(value = "/prime/notaprime/{number}", method = RequestMethod.POST)
  public String notAPrime(@PathVariable("number") int number) {
    if (!(notPrimes.contains(number))) {
      notPrimes.add(number);
      return "added in the non prime database";
    } else
      return "Its already added!";

  }

  @GetMapping("prime/getprimes")
  public ArrayList<Integer> getPrimes() {
    return primes;
  }

  @GetMapping("prime/getnonprimes")
  public ArrayList<Integer> getNonPrimes() {
    return notPrimes;
  }

}
