package com.example.restApi;

import org.springframework.stereotype.Repository;

@Repository
public class PrimeRepoImpl implements PrimeRepository {

  public PrimeRepoImpl() {}

  public boolean isItAPrime(int number) {

    for (int i = 2; i <= number / 2; ++i) {
      // condition for nonprime number
      if (number % i == 0) {
        return false;
      }
    }

    return true;
  }



}
